import React from "react";

import CustomCardList from '../../../components/CustomCardList/CusromCardList';
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import { makeStyles } from "@material-ui/core/styles";

import styles from "assets/jss/material-kit-react/components/typographyStyle.js";
import team1 from "../../../assets/img/faces/faceCard.png";
import team2 from "../../../assets/img/svgBG/videoBG22.png";
import team3 from "../../../assets/img/svgBG/videoBG33.png";
import team4 from "../../../assets/img/svgBG/videoBG44.png";

import { useHistory } from "react-router-dom";
import ViewCoursePage from "../ViewCoursePage/ViewCoursePage";

const useStyles = makeStyles(styles);

export default function CoursesPage({ showArrowIcon , ...props}) {
    
    const classes = useStyles();
    const history = useHistory();
    const [course, setCourse] = React.useState([
        { courseText: 'تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '1', imgSrc: team1, courseSalary: '299' , coins: 'دينار كويتي'},
        { courseText: 'تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '2', imgSrc: team2, courseSalary: '340' , coins: 'دينار كويتي'},
        { courseText: 'دورة صناعة المحتوى مع هند الناهض', id: '3', imgSrc: team3, courseSalary: '500', coins: 'دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '4', imgSrc: team4, courseSalary: '190', coins: 'دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '5', imgSrc: team1, courseSalary: '299', coins: 'دينار كويتي' },
        { courseText: ' تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '6', imgSrc: team2, courseSalary: '299', coins: 'دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '7', imgSrc: team4, courseSalary: '190', coins: 'دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '8', imgSrc: team3, courseSalary: '299' ,coins: 'دينار كويتي'},
        { courseText: ' تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '9', imgSrc: team2, courseSalary: '299', coins: 'دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '10', imgSrc: team1, courseSalary: '190', coins: 'دينار كويتي' },
    ])

   

    
    const [showViewCourse, setShowViewCourse] = React.useState(false);
    
    React.useEffect(() => {
        if(props.hideViewCourse){
            setShowViewCourse(true)
        } else {
            setShowViewCourse(false)
        }
    })

   const  changeActiveTab = (course ) => {
    setShowViewCourse(true)
    showArrowIcon(true)
   }
    return (
        <GridContainer justify="space-between"  >
            <GridItem xs={12} sm={12} md={12} className={showViewCourse === false ? classes.cardContainer   : classes.hideCourses }>
                {course.map((course) => (
                   
                    <CustomCardList courseList={true} containerStyle={'CoursesContainer'} key={course.id} imgSrc={course.imgSrc} courseText={course.courseText} courseSalary={course.courseSalary} coins={course.coins} onClick={() => changeActiveTab(course)}/>
                ))}
            </GridItem>

            <div  className={showViewCourse === true ? classes.showViewCourse : classes.hideViewCourse}>
                <ViewCoursePage  isMobile={props.isMobile} isWeb={props.isWeb} />
            </div>
        </GridContainer>

        
    )
}