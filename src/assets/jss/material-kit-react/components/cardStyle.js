
import imagesStyle from "assets/jss/material-kit-react/imagesStyles.js";
import overlay from "assets/img/overlay.svg"

const cardStyle = {
  card: {
    border: "0", 
    borderRadius: "6px",
    color: "rgba(0, 0, 0, 0.87)",
    background: "#fff",
    width: "100%",
    boxShadow:
      "0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12)",
    position: "relative",
    display: "flex",
    flexDirection: "column",
    minWidth: "0",
    wordWrap: "break-word",
    fontSize: ".875rem",
    transition: "all 300ms linear"
  },
  ...imagesStyle,
  cardPlain: {
    background: "transparent",
    boxShadow: "none",
    minHeight: '200px',
  },
  cardCustom: {
    background: "transparent",
    boxShadow: "none",
    minHeight: '200px',
    height: '260px'
  },
  cardCarousel: {
    overflow: "hidden"
  },
  container: {
    width: "30%",
    height: '250px',
    paddingLeft: '5px',
    paddingRight: '5px',
    marginBottom:'6%',
    "@media (max-width: 640px)": {
      width: "45%",
    },
  },
  fileViewer: {
    height: '25vh',
    background: '#153064',
    objectFit: 'contain',
    borderRadius: '5px',
    backgroundImage: 'linear-gradient(312deg, #10306f 11%, rgb(183 185 191 / 13%) 40%)'
  },

  overlay: {
  position: 'absolute',
  bottom: '1vw',
  width: '65%',
  opacity: '1',
  },
  imgOverlay: {
      height: '51vh',
      width: '100%',
      position: 'absolute',
      bottom: '0px',
      opacity: '1.8',
  },
  bgOverlay:{
      height: '50vh',
      position: 'absolute',
      top: 'calc(23vh + 5px)',
      width: '65%',
      opacity: '0.7',
      background: "url(assets/img/overlay.svg)",
      backgroundSize: 'cover'
  },
  fileLabel:{
    color: 'white',
    right: '35px',
    width: '28%',
    bottom: '0vw',
    opacity: '1',
    zIndex: '200',
    position: 'absolute'
  },
  iconFLoat:{
    width: '10%',
    bottom: '1vw',
    opacity: '1',
    position: 'absolute',
    zIndex:'200',
    right: '8px'
  },
  Bgoverlay:{
    width: '100%',
    bottom: '0px',
    opacity: '0.8',
    position: 'absolute',
    right: '-1px'
  },

  overLabel:{
  position: 'absolute',
  top: '350px',
  width: '50%',
  opacity: '1',
  fontSize: '25px',
  color: 'white',
  zIndex: '288',
  height: '5vh'
  },
  CoursesContainer: {
    width: "25%",
    height: '300px',
    paddingLeft: '30px',
    paddingRight: '30px',
    "@media (max-width: 640px)": {
      width: "100%",
      height: '44vh',
      paddingLeft: '2vw',
      paddingRight: '2vw',
    },
  },

  videoPlayer: {
      borderRadius: '12px',
      marginBottom: '42px',  
    
  },
  videoPlayerMobile: {
    display: 'none'
  },
  cardTitle: {
    color: '#ffffff',
    fontSize: '14px',
    lineHeight: '1.43'
  },
  smallTitle: {
    color: '#8191b2',
    fontSize: '13px',
    lineHeight: '1.53',
   
  
  },
  customSmallTitle: {
    color: '#8191b2',
    fontSize: '13px',
    lineHeight: '1.53',
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  icon: {
    color: '#e57a3c'
  }
};

export default cardStyle;
